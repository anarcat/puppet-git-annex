Exec {
  path => '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin',
}

class { 'gitannex':
  target => '/tmp/annex',
  method => 'git-annex';
}

gitannex::repo { '/tmp/foo':
}
