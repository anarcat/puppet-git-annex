#!/usr/bin/python
# -*- coding: utf-8

from __future__ import print_function

'''security checks for a public git-annex repository

the following variables control the behaviour of this hook:

* annex.protectfiles: (string) space-separated list of files that should
  not be modified in any way. default is to allow all changes. exit
  status 2 if modifications found.

* annex.protectfilesref: (string) space-separated list of refs that
  should be checked for modifications. default is to check all refs.

* annex.protectremoval: (bool) forbid removals of any files, exit
  status 1 if removal found. default is to not forbid removal.

* annex.protectremovalref: (string) space-separated list of refs that
  should be checked for removals. default is to check all refs.

More information about this in:

http://git-annex.branchable.com/todo/git-hook_to_sanity-check_git-annex_branch_pushes/

'''

import argparse
import copy
import logging
import os.path
import pygit2
import shlex
import shutil
import sys
import tempfile

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)

class TestRepo(pygit2.Repository):
    '''helper class for unit tests

    this will init a new repository in a temporary directory then
    destroy it when the object is garbage-collected

    >>> repo = TestRepo()
    >>> os.path.isdir(repo.path)
    True
    >>> os.path.isdir(os.path.join(repo.workdir, '.git'))
    True
    '''
    def __new__(cls):
        '''override object creation

        we want to use pygit2.init_repository() because we want to
        create a new repo anyways. then we hijack the class parameter
        to make sure our destructor is called in the end
        '''
        path = tempfile.mkdtemp()
        repo = pygit2.init_repository(path)
        repo.__class__ = TestRepo
        return repo

    def __init__(self):
        '''override constructor to ignore require path argument

        this is because we already built the object in __new__()

        >>> repo = TestRepo()
        >>> os.path.isdir(repo.path)
        True
        >>> os.path.isdir(os.path.join(repo.workdir, '.git'))
        True
        '''
        pass

    def __del__(self):
        '''
        remove the temporary repository

        >>> repo = TestRepo()
        >>> p = copy.copy(repo.path)
        >>> del repo
        >>> os.path.isdir(p)
        False
        '''
        shutil.rmtree(self.path)

    def _testcreatecommit(self, path,
                          content='',
                          log='test',
                          parents=[],
                          ref='HEAD'):
        '''
        >>> repo = TestRepo()
        >>> c = repo._testcreatecommit('README.txt')
        >>> c is not False
        True
        >>> repo.get(str(c)) is not None
        True
        >>> repo._testcreatecommit('README.txt', 'hello world', 'test', [c]) is not None
        True
        '''
        author = committer = self.default_signature
        blob = self.write(pygit2.GIT_OBJ_BLOB, content)
        bld = self.TreeBuilder()
        bld.insert(path, blob, pygit2.GIT_FILEMODE_BLOB)
        tree = bld.write()
        return self.create_commit(ref, author, committer, log, tree, parents)

    def _testremove_file(self, path, log='test', parents=None, ref='HEAD'):
        '''
        >>> repo = TestRepo()
        >>> c = repo._testcreatecommit('README.txt', 'hello world')
        >>> repo._testremove_file('README.txt') is not None
        True
        '''
        author = committer = self.default_signature
        oid = self.revparse_single(ref).id
        if parents is None:
            parents = [oid]
        self.reset(oid, pygit2.GIT_RESET_HARD)
        index = self.index
        index.read()
        index.remove(path)
        index.write()
        tree = index.write_tree(self)
        return self.create_commit(ref, author, committer, log, tree, parents)

def find_removals(repo, base, commit):
    '''check to see if any file was removed
    
    >>> repo = TestRepo()
    >>> c1 = repo._testcreatecommit('README.txt', 'hello worlds', 'test commit 2')
    >>> c2 = repo._testcreatecommit('README.txt', 'hello world 2', 'test commit 2', [str(c1)])
    >>> find_removals(repo, c1, c2)
    []
    >>> c3 = repo._testremove_file('README.txt')
    >>> find_removals(repo, c1, c3)
    ['README.txt']
    '''
    base = repo.get(base).tree
    commit = repo.get(commit).tree
    return [p.old_file_path for p in repo.diff(base, commit) if p.new_id == '0000000000000000000000000000000000000000']

def find_modified(repo, base, commit):
    '''check to see if some files were modified

    >>> repo = TestRepo()
    >>> c1 = repo._testcreatecommit('README.txt', 'hello worlds', 'test commit 2')
    >>> c2 = repo._testcreatecommit('README.txt', 'hello world 2', 'test commit 2', [str(c1)])
    >>> find_modified(repo, c1, c2)
    ['README.txt']
    >>> c3 = repo._testremove_file('README.txt')
    >>> find_modified(repo, c1, c3)
    ['README.txt']
    '''
    base = repo.get(base).tree
    commit = repo.get(commit).tree
    return [p.new_file_path for p in repo.diff(base, commit)]

def process_stream(repo, stream=sys.stdin):
    for line in stream:
        (base, commit, ref) = line.strip().split()
        try:
            protectremoval = repo.config.get_bool('annex.protectremoval')
        except KeyError:
            protectremoval = False
        protectedref = shlex.split(" ".join(repo.config.get_multivar('annex.protectremovalref')))
        if not protectedref or ref in protectedref and protectremoval:
            removed = find_removals(repo, base, commit)
            if len(removed) > 0:
                logging.warning("found removed files on ref {}, refusing commit: {}".format(ref, removed))
                sys.exit(1)
        protectedref = shlex.split(" ".join(repo.config.get_multivar('annex.protectfilesref')))
        if not protectedref or ref in protectedref:
            # extract the list of protected files to make "quoted file" work
            protected = shlex.split(" ".join(repo.config.get_multivar('annex.protectfiles')))
            files = find_modified(repo, base, commit)
            modif = set(protected).intersection(protected)
            if len(modif) > 0:
                logging.warning("protected files modified on {}, refusing commit: {}".format(ref, list(modif)))
                sys.exit(2)

if __name__ == '__main__':
    path = pygit2.discover_repository(os.getcwd())
    process_stream(pygit2.Repository(path))
