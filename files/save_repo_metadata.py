#!/usr/bin/python
# -*- coding: utf-8

from __future__ import print_function

'''fetch metadata from system and store it in the git-annex branch

this script will fetch the public and private IP addresses of this
system and store it in the git-annex branch of the repository
provided on the commandline.

unit tests can be ran with:

python -m doctest __FILE__

this script can be installed anywhere in your path. It should work in
Python 2 and 3, but needs the pygit library::

    if ! sudo apt-get install python-pygit2 ; then
      sudo apt-get install libgit2-dev python-dev python-pip python-cffi
      sudo pip install "pygit2 == 0.21" # make sure it's the same version as above
    fi
'''

__version_info__ = ('1', '0')
__version__ = '.'.join(__version_info__)
__author__ = 'Antoine Beaupré'
__email__ = 'anarcat@koumbit.org'
__copyright__ = '(C) 2015 %s <%s>' % (__author__, __email__)
__warranty__ = '''This program comes with ABSOLUTELY NO WARRANTY.  This is free
software, and you are welcome to redistribute it under certain
conditions; see `--copyright` for details.'''
__license__ = '''This program is free software: you can redistribute
it and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3
of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program. If not, see
<http://www.gnu.org/licenses/>.
'''

import argparse
import copy
import json
import logging
import os.path
import pygit2
import re
import shutil
import socket
import subprocess
import sys
import tempfile
try:
    # python3
    import urllib.request as urllib
except ImportError:
    # python2
    import urllib2 as urllib

def filter_annex_metadata(data,
                          uuid,
                          **kwargs):
    '''maintain string to add/replace given metadata fields

    i am not happy with this dumb implementation. maybe it would be
    cleaner to actually parse the file by tokenizing it into a dict
    and changing what we need. then we would need to be careful with
    order. right now this seems to preserve order.

    >>> data = ''
    >>> data = filter_annex_metadata(data, 'uuid', internal_ip='10.0.0.1')
    >>> data.strip()
    'uuid internal_ip=10.0.0.1'
    >>> data = filter_annex_metadata(data, 'uuid', external_ip='10.0.0.1')
    >>> data.strip()
    'uuid internal_ip=10.0.0.1 external_ip=10.0.0.1'
    >>> data = filter_annex_metadata(data, 'uuid', external_ip='10.0.0.2')
    >>> data.strip()
    'uuid internal_ip=10.0.0.1 external_ip=10.0.0.2'
    >>> data = filter_annex_metadata(data, 'uuid', internal_ip='10.0.0.3')
    >>> data.strip()
    'uuid internal_ip=10.0.0.3 external_ip=10.0.0.2'
    >>> data = filter_annex_metadata(data, 'uuid2', internal_ip='10.0.0.3')
    >>> data
    'uuid internal_ip=10.0.0.3 external_ip=10.0.0.2\\nuuid2 internal_ip=10.0.0.3\\n'

    '''
    if uuid in data:
        lines = data.split("\n")
        for i in range(len(lines)):
            if uuid in lines[i]:
                line = lines[i]
                logging.debug('editing line %s', line)
                for k, v in kwargs.items():
                    if k + "=" in line:
                        patt = r'%s=[^ ]*( |$)' % k
                        repl = "%s=%s " % (k,v)
                        logging.debug('found key %s, replacing with %s (%s => %s', k, v, patt, repl)
                        line = re.sub(patt, repl, line).strip()
                    else:
                        line = line.strip() + " %s=%s" % (k,v)
                lines[i] = line
        data = "\n".join(lines)
    else:
        # simple case: append all
        logging.debug('uuid %s missing, appending', uuid)
        data += uuid
        for k, v in kwargs.items():
            data += ' %s=%s' % (k,v)
        data += "\n"
    return data

def read_remote(repo,
                ref='refs/heads/git-annex',
                path='remote.log'):
    '''read the given file from git on the given branch

    returns the empty string if nothing is found'''
    try:
        tree = repo.revparse_single(ref).tree[path]
    except KeyError as e:
        lines = ''
    else:
        lines = repo[tree.id].data
    return lines

def write_remote(repo, data,
                 ref='refs/heads/git-annex',
                 path='remote.log'):
    '''
    write the given data to a file in a git branch

    this will synthesise a commit using the git-annex index. it will
    *not* check to see if the file has changed (but should)

    >>> repo = TestAnnexRepo()
    >>> u = repo.config['annex.uuid']
    >>> data = read_remote(repo)
    >>> remote = filter_annex_metadata(data, u, internal_ip='10.0.0.1')
    >>> if data != remote:
    ...     result = write_remote(repo, remote)
    '''
    parent = repo.revparse_single(ref)
    author = committer = repo.default_signature
    index = pygit2.Index(os.path.join(repo.path, 'annex', 'index'))
    oid = repo.create_blob(data)
    logging.debug('created new blob %s', oid)
    entry = pygit2.IndexEntry(path, oid, pygit2.GIT_FILEMODE_BLOB)
    index.add(entry)
    index.write()
    tree = index.write_tree(repo)
    logging.debug('creating commit based on tree %s and parent %s', tree, parent.id)
    commit = repo.create_commit(ref, author, committer, 'saving metadata fields', tree, [parent.id])
    return commit

def ensure_uuid_keys(repo, uuid, **kwargs):
    '''ensure that the given metadata is associated with the given repository

    this will return the commit id if there were changes in the
    metadata, or None if no changes were done.

    >>> repo = TestAnnexRepo()
    >>> u = repo.config['annex.uuid']
    >>> commit = ensure_uuid_keys(repo, u, internal_ip='10.0.0.1')
    >>> ensure_uuid_keys(repo, u, internal_ip='10.0.0.1')
    >>> new = ensure_uuid_keys(repo, u, internal_ip='10.0.0.2')
    >>> commit != new
    True
    '''
    logging.debug('setting parameters on %s: %s', uuid, kwargs)
    data = read_remote(repo)
    new = filter_annex_metadata(data, uuid, **kwargs)
    logging.debug('remote.log before: %s, after: %s',
                  data.strip(),
                  new.strip())
    if data != new:
        return write_remote(repo, new)

class TestAnnexRepo(pygit2.Repository):
    '''helper class for unit tests

    this will init a new repository in a temporary directory then
    destroy it when the object is garbage-collected

    >>> repo = TestAnnexRepo()
    >>> os.path.isdir(repo.path)
    True
    >>> os.path.isdir(os.path.join(repo.workdir, '.git', 'annex'))
    True
    '''
    def __new__(cls):
        '''override object creation

        we want to use pygit2.init_repository() because we want to
        create a new repo anyways. then we hijack the class parameter
        to make sure our destructor is called in the end
        '''
        path = tempfile.mkdtemp()
        repo = pygit2.init_repository(path)
        repo.__class__ = TestAnnexRepo
        return repo

    def __init__(self):
        '''override constructor to ignore require path argument

        this is because we already built the object in __new__()

        we also need to initialise the repo as a git-annex repo here

        >>> repo = TestAnnexRepo()
        >>> os.path.isdir(repo.path)
        True
        >>> os.path.isdir(os.path.join(repo.workdir, '.git', 'annex'))
        True
        '''
        p = self.workdir
        if p is None:
            p = self.path
        subprocess.call(['git', '-C', p, 'annex', 'init'])

    def __del__(self):
        '''
        remove the temporary repository

        >>> repo = TestAnnexRepo()
        >>> p = copy.copy(repo.path)
        >>> del repo
        >>> os.path.isdir(p)
        False
        '''
        shutil.rmtree(self.path)

def find_internal_address(addr = '169.254.0.1'):
    '''
    >>> find_internal_address('127.0.0.2')
    '127.0.0.1'
    >>> find_internal_address() in subprocess.check_output(['ifconfig'])
    True
    '''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # address doesn't seem to matter here, as long as it's not loop
    # back, it will just fail to connect (or not) but still choose the
    # proper interface based on the routing table, which will most
    # likely be the default route
    #
    # the 169.254.0.0/16 netblock is chosen because it is used in
    # replacement for DHCP when it fails. it is garanteed to not be
    # used on the network 
    s.connect((addr,0))
    addr = s.getsockname()[0]
    s.close()
    return addr

def find_public_address():
    '''find the public IP address of this network

    we assume that the first of these sites that answers in time will
    give the right address. we do validate the result, but we trust
    that it is the correct address.

    if we would prefer a more reliable approach, we could check all of
    those sites simultaneously and verify they all give the same
    answer instead.
    '''

    methods = [ find_public_address_httpbin, find_public_address_raws ]
    for method in methods:
        try:
            logging.debug('trying method %s', method)
            addr = method().strip()
            # check if IP is valid, will raise socket.error otherwise
            socket.inet_aton(addr)
            return addr
        except (urllib.HTTPError, urllib.URLError, socket.error) as e:
            logging.warning('method %s failed with exception %s', method, e)

def find_public_address_httpbin():
    '''get our public IP from the HTTPbin.org public JSON interface

    this could be extended to support also http://getjsonip.com/ which
    also supports IPv6, but it doesn't have the same interface (the
    field is `ip` instead of `origin`).

    ifconfig.me also has a JSON interface but it's generally slower
    than httpbin.org in my tests, and the interface is *also*
    different (field is `ip_addr`).

    we do not check if the IP is valid here.
    '''
    return json.load(urllib.urlopen('http://httpbin.org/ip',timeout=1))['origin']

def find_public_address_raws():
    '''get our public IP from one of many public sites

    we expect those sites to return the IP address in plain text in
    the body of the HTTP response.

    if one doesn't respond correctly in sufficient time, we skip it
    and go to the next one. we do not check if the IP is valid here.
    '''

    raws = ['http://ip.42.pl/raw', 'http://ifconfig.me/ip']
    for site in raws:
        try:
            logging.debug('trying site %s', site)
            return urllib.urlopen(site, timeout=1).read()
        except (urllib.HTTPError, urllib.URLError) as e:
            logging.warning('site %s failed with exception %s', site, e)

def parse_args():
    parser = argparse.ArgumentParser(description=__copyright__ + "\n" + __warranty__,
                                     epilog=__doc__)
    parser.add_argument('-v', '--verbose', action='count',
                        help=("output more information on"
                              "console. silent by default"))
    parser.add_argument('--copyright', action='store_true',
                        help='''display copyright notice and exit''')
    parser.add_argument('--version', action='store_true',
                        help='''display version number and exit''')
    parser.add_argument('-l', '--logfile', default=sys.stdout,
                        help="""file where logs should be written,
                        defaults to stdout""")
    levels = [i for i in logging._levelNames.keys()
              if (type(i) == str and i != 'NOTSET')]
    parser.add_argument('--syslog', nargs='?', default=None,
                        type=str.upper, action='store',
                        const='INFO', choices=levels,
                        help="""log to syslog facility, default: no
                        logging, INFO if --syslog is specified without
                        argument""")
    parser.add_argument('--repository', default='.',
                        help='''git-annex repository to store metdata''')
    parser.add_argument('--external_ip', default=None,
                        help='''override external IP detection code''')
    parser.add_argument('--internal_ip', default=None,
                        help='''override internal IP detection code''')
    args = parser.parse_args()
    if args.copyright:
        parser.exit(0, __license__)
    if args.version:
        parser.exit(0, __version__ + "\n")
    return args

def setup_logging(args):
    # setup python logging facilities
    if args.syslog:
        sl = logging.handlers.SysLogHandler(address='/dev/log')
        sl.setFormatter(logging.Formatter('irklab%(process)d]: %(message)s'))
        # convert syslog argument to a numeric value
        loglevel = getattr(logging, args.syslog.upper(), None)
        if not isinstance(loglevel, int):
            raise ValueError('Invalid log level: %s' % loglevel)
        sl.setLevel(loglevel)
        logging.getLogger('').addHandler(sl)
        logging.debug('configured syslog with level %s' % logging.getLevelName(loglevel))
    # log everything in main logger
    logging.getLogger('').setLevel(logging.DEBUG)
    if args.logfile == sys.stdout or args.logfile == '/dev/stdout':
        sh = logging.StreamHandler()
        if args.verbose > 1:
            sh.setLevel(logging.DEBUG)
        elif args.verbose > 0:
            sh.setLevel(logging.INFO)
        else:
            sh.setLevel(logging.WARNING)
        logging.getLogger('').addHandler(sh)
        logging.debug('configured stdout with level %s' % logging.getLevelName(sh.level))
    else:
        # keep 52 weeks of logs
        fh = logging.handlers.TimedRotatingFileHandler(args.logfile, when='W6', backupCount=52)
        # serve back the stream to other processes
        logging.getLogger('').addHandler(fh)
        logging.debug('configured file output to %s with level %s' % (args.logfile, logging.getLevelName(sh.level)))

if __name__ == '__main__':
    args = parse_args()
    setup_logging(args)
    d = {}
    d['internal_ip'] = args.internal_ip
    d['external_ip'] = args.external_ip
    if not args.internal_ip:
        d['internal_ip'] = find_internal_address()
    if not args.external_ip:
        d['external_ip'] = find_public_address()

    try:
        repo = pygit2.Repository(args.repository)
        uuid = repo.config['annex.uuid']
    except KeyError as e:
        logging.error('repository %s is not a git-annex repository: %s', args.repository, repr(e))
        sys.exit(1)
    commit = ensure_uuid_keys(repo, uuid, **d)
    if commit:
        logging.info("saved metadata %s into git-annex commit %s", d, commit)
    else:
        logging.info("no change detected in IP addresses, nothing committed")
