define gitannex::remote($repo, $source, $branch = undef) {
  ini_setting {
    "git-config-remote-url-$name":
      ensure => present,
      path   => "$repo/.git/config",
      section => "remote \"$name\"",
      setting => 'url',
      value   => $source;
    "git-config-remote-fetch-$name":
      ensure => present,
      path   => "$repo/.git/config",
      section => "remote \"$name\"",
      setting => 'fetch',
      value   => "+refs/heads/*:refs/remotes/$name/*",
  }
  if $branch {
    ini_setting {
      "git-config-branch-remote-$branch":
        ensure => present,
        path   => "$repo/.git/config",
        section => "branch \"$branch\"",
        setting => 'remote',
        value   => $name;
      "git-config-branch-merge-$branch":
        ensure => present,
        path   => "$repo/.git/config",
        section => "branch \"$branch\"",
        setting => 'merge',
        value   => "refs/heads/$branch";
    }
  }
}
