# special git-annex configuration that runs our custom script to
# populate metadata info in the git-annex branch
class gitannex::metadata (
  $repository = '/var/lib/git-annex/repo',
  $user = 'git-annex'
) {
  file { '/usr/local/bin/save_repo_metadata':
    source => 'puppet:///modules/gitannex/save_repo_metadata.py',
    mode   => '0555',
  }
  # run job every 5 minutes
  cron { 'metadata':
    command => "/usr/local/bin/save_repo_metadata ${repository}",
    minute  => '*/5',
    user    => $user,
  }
  package { 'pygit2':
    ensure => present,
    provider => 'pip',
    require  => [
                 Package['libgit2-dev'],
                 Package['python-dev'],
                 Package['python-pip'],
                 Package['python-cffi'],
                 Package['libffi-dev'],
                 ],
  }
  package { ['libgit2-dev', 'python-dev', 'python-pip', 'python-cffi', 'libffi-dev']:
    ensure => present,
  }
}
