# Setup the git-annex daemon to run with a particular repository.
#
# TODO add missing documentation for arguments
# Arguments:
#
# * $sched_start_hour:
#     At which hour should git-annex start to sync files. If this argument is
#     set, then $sched_stop_hour should also be set.  This argument is passed
#     to the cron 'hour' parameter, so it should respect syntax for it. It can
#     be an array of integers or an array containing one string that uses the
#     crontab syntax for hours (e.g.  ['2-4'])
# * $sched_start_minute:
#     At which minute(s) in the hour should git-annex start to sync files.
#     Setting only this parameter without setting $sched_start_hour has no
#     effect. This argument is passed to the cron 'minute' parameter so it
#     should respect syntax for it. This argument is a string that uses the
#     crontab syntax for minutes (e.g. '*/10' for every ten minutes).
# * $sched_stop_hour:
#     At which hour should git-annex stop sync'ing files. This will not shut
#     down the daemon, since this would prevent it from synchronizing metadata;
#     it will only prevent the daemon from downloading files from the special
#     remote "web". If this argument is set, then $sched_start_hour should also
#     be set.  This argument is passed to the cron 'hour' parameter. See
#     $sched_start_hour for a description of its syntax.
# * $sched_stop_minute:
#     At which minute(s) in the hour should the git-annex daemon be stopped.
#     Setting only this parameter without setting $sched_stop_hour has no
#     effect. This argument is passed to the cron 'minute' parameter. See
#     $sched_start_minute for a description of its syntax.
#
class gitannex::daemon(
  $daemon = '/usr/bin/git-annex',
  $repository = '/var/lib/git-annex/repo',
  $user = 'git-annex',
  $groups = [],
  $sched_start_hour = [],
  $sched_start_minute = absent,
  $sched_stop_hour = [],
  $sched_stop_minute = absent
) {

  # we deliberately don't create the user according to the class,
  # because we may want to reuse an existing user...
  user { 'git-annex':
    system => true,
    comment => 'Git annex sandbox',
    home => '/var/lib/git-annex',
    groups => $groups,
    shell => "${daemon}-shell";
  }
  file {
    '/var/lib/git-annex':
      ensure => directory,
      owner  => 'git-annex';
    '/etc/init.d/git-annex':
      mode   => '0555',
      source => 'puppet:///modules/gitannex/init.d';
    '/etc/default/git-annex':
      mode    => '0444',
      content => "DAEMON=${daemon}\nANNEX=${repository}\nUSER=${user}\n";
  }

  if ! empty($sched_start_hour) {
    if ! empty($sched_stop_hour) {
      cron { 'git-annex_start':
        ensure  => present,
        command => "GIT_DIR=${repository} git config remote.web.annex-ignore false",
        user    => 'root',
        hour    => $sched_start_hour,
        minute  => $sched_start_minute,
      }
      cron { 'git-annex_stop':
        ensure  => present,
        command => "GIT_DIR=${repository} git config remote.web.annex-ignore true",
        user    => 'root',
        hour    => $sched_stop_hour,
        minute  => $sched_stop_minute,
      }
    }
    else {
      fail('$sched_start_hour is set but not $sched_stop_hour. They should both be set simultaneously, otherwise it doesn\'t make sense.')
    }
  }
  else {
    if ! empty($sched_stop_hour) {
      fail('$sched_stop_hour is set but not $sched_start_hour. They should both be set simultaneously, otherwise it doesn\'t make sense.')
    }
    else {
      # We don't want to keep those around. If we decide at some point to
      # remove the schedule on a given host, the cronjobs will disrupt service.
      cron { ['git-annex_start', 'git-annex_stop']:
        ensure => absent,
      }

      # No schedule, ensure service is always running
      service { 'git-annex':
        ensure  => running,
        enable  => true,
        require => [
            File['/etc/init.d/git-annex'],
            File['/etc/default/git-annex']
          ],
      }
    }
  }

}
