define gitannex::repository(
  $owner,
  $ensure = 'present',
) {
    vcsrepo { "$name":
      ensure => $ensure,
      owner  => $owner,
      provider => git,
    }
    exec { "git annex init":
      creates => "$name/.git/annex",
      cwd     => $name,
      user    => $owner,
    }
}
