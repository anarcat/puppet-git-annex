class gitannex ($target = '/opt',
$source = 'downloads.kitenet.net',
$method = 'package',
$ensure = present,
$region = 'us-nh') {
  $os = downcase($kernel)
  $arch = $architecture
  if $method == 'package' {
    package {
      'git-annex-standalone':
        ensure => $ensure,
        require => Apt::Sources_list['neurodebian'],
    }
    apt::sources_list {
      'neurodebian':
        source => [ "puppet:///modules/site_gitannex/${::lsbdistcodename}.${region}.libre.list",
                    "puppet:///modules/gitannex/${::lsbdistcodename}.${region}.libre.list" ]
    }
    apt::preferences_snippet {
      'git-annex-standalone':
        # override the distribution package
        priority => 990,
        pin      => 'origin NeuroDebian';
      'disable_neurodebian':
        package  => '*',
        # disable neurodebian, unless explicitely requested
        priority => 1,
        pin      => 'origin NeuroDebian';
    }
    apt::key {
      'neurodebian.gpg':
        source => 'puppet:///modules/gitannex/neurodebian.gpg',
    }
    $binary = 'git-annex'
    file {
      [
       "$target/$source",
       "$target/git-annex.$os",
       ]:
        ensure  => 'absent',
        purge   => true,
        recurse => true,
        force   => true,
    }
        
  }
  elsif $method == 'git-annex' {
    package { 'git-annex': ensure => 'absent'; }
    vcsrepo { "$target/$source":
      ensure => $ensure,
      source => "http://$source/.git/",
      provider => git;
    }
    $url = "http://$source/git-annex/$os/current/git-annex-standalone-$arch.tar.gz"
    $tarfile = "$target/$source/git-annex/$os/current/git-annex-standalone-$arch.tar.gz"
    $binary = "$target/git-annex.$os/git-annex"
    exec {
      "mkdir -p $(dirname $(readlink 'git-annex-standalone-$arch.tar.gz')) ; wget -O $(readlink 'git-annex-standalone-$arch.tar.gz') '$url'":
        alias => 'gitannex::download',
        require => Vcsrepo["$target/$source"],
        creates => $tarfile,
        unless => "[ -x $binary ] && $binary get 'git-annex-standalone-$arch.tar.gz'",
        cwd => "$target/$source/git-annex/$os/current/";
      "tar -C $target -x -z -f '$target/$source/git-annex/$os/current/git-annex-standalone-$arch.tar.gz'":
        alias => 'gitannex::decompress',
        require => Exec['gitannex::download'],
        creates => $binary,
        cwd => $target;
    }
  }
  else {
    err("gitannex unknown method: $method")
  }
}
