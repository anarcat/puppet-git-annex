define gitannex::repo ( $ensure = present, $source = undef ) {
  vcsrepo { $name:
    ensure => $ensure,
    source => $source,
    provider => git;
  }
  exec { "git-annex init $name":
    command => "$gitannex::binary init",
    require => Vcsrepo[$name],
    cwd => $name,
    creates => "$name/.git/annex";
  }
}
    
