define gitannex::bandwidth($upload, $download) {
  ini_setting {
    "git-config-annex-download-$name":
      ensure => present,
      path   => "$name/.git/config",
      section => "annex",
      setting => 'web-download-command',
      value   => "wget -q --show-progress --clobber -c --limit-rate=$download -O %file %url";
    "git-config-annex-upload-$name":
      ensure => present,
      path   => "$name/.git/config",
      section => "annex",
      setting => 'rsync-upload-options',
      value   => "--bwlimit $upload";
  }
}
