Git-annex Puppet module
=======================

This module can be used to do basical manipulations in git-annex:
install it as a package or from snapshots, create repositories, manage
the assistant.

Usage
-----

To install git-annex, just use:

    include gitannex

This will use packages by default, so it will install whatever is the
latest available version in your repository. To install from
standalone tarballs:

    class {
        'gitannex':
            destination => '/opt',
            method => 'git-annex';
    }

Use this to keep the package or the git-annex repo up to date:

    class { 'gitannex': ensure => 'latest'; }

It should work with both the package and the `gitannex` method,
although the latter wasn't tested.

To make a git-annex repository, use:

    gitannex::repo { '/path/to/repo': }

<!-- To run git-annex within a repo, use:

    gitannex::assistant { '/path/to/repo': }

This will also initialize the repository if not done already. -->

Requirements
------------

This requires the following modules from the forge:

* stdlib
* vcsrepo
